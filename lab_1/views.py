from django.shortcuts import render

# Create your views here.
from datetime import datetime, date
# Enter your name here
mhs_name = 'Michael'
age = 19
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 2, 1)
npm = 1706039944
kuliah = "Universitas Indonesia"
hobi = {"Makan", "Main", "Belajar"}
deskrpisi = "Halo, nama saya Michael, saat ini saya sedang mengikuti perkuliahan di Fasilkom UI, " \
           "Saya sangat suka makan dan bermain."
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': age, 'npm': npm, 'kuliah': kuliah, 'hobi': hobi, 'deskripsi' : deskrpisi}
    return render(request, 'index_lab1.html', response)

